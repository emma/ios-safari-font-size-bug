import Ticker from "react-ticker";

export default function Index() {
	return (
		<div className="wrapper">
			<p>Rendered client-side:</p>
			<Ticker move={false} height="">
				{() => (
					<>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Pellentesque vehicula auctor vulputate. Donec molestie velit ante,
						et eleifend ex vehicula vel. Donec eu aliquam libero.
						{}
						&nbsp;
					</>
				)}
			</Ticker>

			<p>Rendered server-side:</p>
			{/* Copied from the rendered DOM of the client-side rendered version on emulated iPhone 8 */}
			<div
				className="ticker"
				style={{ position: "relative", overflow: "hidden", height: "27px" }}
			>
				<div
					className="ticker__element"
					style={{
						willChange: "transform",
						position: "absolute",
						left: "0px",
						top: "0px",
						transform: "translate3d(0px, 0px, 0px)"
					}}
				>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
					vehicula auctor vulputate. Donec molestie velit ante, et eleifend ex
					vehicula vel. Donec eu aliquam libero.
					{}
					&nbsp;
				</div>
				<div
					className="ticker__element"
					style={{
						willChange: "transform",
						position: "absolute",
						left: "0px",
						top: "0px",
						transform: "translate3d(359px, 0px, 0px)"
					}}
				>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque
					vehicula auctor vulputate. Donec molestie velit ante, et eleifend ex
					vehicula vel. Donec eu aliquam libero.
					{}
					&nbsp;
				</div>
			</div>
		</div>
	);
}
